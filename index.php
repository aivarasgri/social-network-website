<?php include "connection.php"; ?>

<?php

if(isset($_POST['insert-data'])){
    
    if(isset($_POST['input-email']) && isset($_POST['input-password'])){
        
        $email = $_POST['input-email'];
        $password = $_POST['input-password'];       
        
//       EMAIL DUPLICATION CHECK    
        
       $sql = "SELECT email FROM login_profile WHERE email='$email'" ;

       $result = mysqli_query($connection, $sql ) ;

       if( mysqli_num_rows( $result ) > 0 ){
           
           echo '<div class="alert alert-danger" role="alert">
                    <h3 class="text-center">Email is Already In Use!</h3>
                </div>';
           
          } else {
       
        session_start(); 
           
//        INSERT REGISTRATION DATA QUERY   
        
        $insertData = "INSERT INTO login_profile SET email = '$email', password = '$password' ";
        $updateQuery = mysqli_query($connection, $insertData);
        
        $_SESSION['email'] = $email;
        
        header('Location: profile.php');
        
        if(!$updateQuery){
            
             die("<h2 class='msg-failed'>Registration Failed!</h2>" . mysqli_error($connection)); 
        
        } 

    }
    
    $query = "SELECT * from login_profile WHERE email = '$email' ";
      $select_user_query = mysqli_query($connection, $query);
      if(!$select_user_query){
          die("ERROR ERROR ERROR " . mysqli_error($connection));
      }
      
      while($row = mysqli_fetch_assoc($select_user_query)){
          
          $db_id = $row['id'];
          $db_email = $row['email'];
          $bd_password = $row['password'];
          
      }
    }
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    
    
<!--    REGISTER FORM-->       
<div class="container col-lg-4 offset-lg-4 border mt-5 ">
   <h1 class="text-center bg-primary text-white rounded font-weight-normal form-header"><p class="p-form">REGISTER</p></h1>
    <form class="pb-3 px-3" action="index.php" method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="input-email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="input-password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
      </div>
      <button type="submit" name="insert-data" class="btn btn-primary">Submit</button><br><br>
      <p>
        Already Registered? <a href="login.php">Login Here!</a>  
      </p>
    </form>
</div> 
<!--    REGISTER FORM-->   
    

        
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>    
</body>
</html>