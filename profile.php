<?php include "connection.php";  ?>
<?php session_start(); ?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>

    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

<!--NAVIGATION BAR -->
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
  <form action="" method="post">
      <input type="submit" class="btn btn-primary" name="logout" value="LOGOUT"><br>
  </form>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<br>
<!--NAVIGATION BAR -->


<!--SUCCESSFULL LOGIN ALERT-->
<h3 class="page-header text-center alert alert-success">
      Welcome, <?php echo  $_SESSION['email']; ?>. Please, create Your profile. 
      <button class="btn1">X</button>
</h3>
<!--SUCCESSFULL LOGIN ALERT-->


<!--LOGIN QUERY-->
<?php
 
  if(isset($_POST['login'])){
    
        $email = $_POST['input-email'];
        $password = $_POST['input-password'];
    
        $email =  mysqli_real_escape_string($connection,$email);   
        $password =  mysqli_real_escape_string($connection,$password);   
      
        $query = "SELECT * from login_profile WHERE email = '$email' ";
        $select_user_query = mysqli_query($connection, $query);
      
      if(!$select_user_query){
          die("ERROR ERROR ERROR " . mysqli_error($connection));
      }
      
      while($row = mysqli_fetch_assoc($select_user_query)){
          
          $db_id = $row['id'];
          $db_email = $row['email'];
          $bd_password = $row['password'];
          
      }
      
      if($email !== $db_email && $password !== $bd_password){
          
          header('Location: login.php');
          
      } else if($email == $db_email && $password == $bd_password) {
                    
          $_SESSION['email'] = $db_email;
       
          
          header('Location: profile.php');
          
      } else {
          
          header('Location: login.php');
          
          
      }
      
    }
  
?>
<!--LOGIN QUERY-->  
  
<!--LOGOUT QUERY-->  
<?php

    if(isset($_POST['logout'])){ 
        unset($_SESSION['email']);
        $_SESSION['email'] = null;
        session_destroy();
        header('Location: login.php');
}

?>
<!--LOGOUT QUERY--> 
  

<!--SELECT QUERY FOR INPUTS    -->
<?php
    
      $query = "SELECT * from login_profile WHERE email = '" . $_SESSION['email'] . "' ";
      $select_user_query = mysqli_query($connection, $query);
      if(!$select_user_query){
          die("ERROR ERROR ERROR " . mysqli_error($connection));
      }
      
      while($row = mysqli_fetch_assoc($select_user_query)){
          
          $id = $row['id'];
          $firstname = $row['name'];
          $lastname = $row['lastname'];
          $image = $row['image'];
          $aboutMe = $row['about_me'];
          
?>
 
<!--UPDATE QUERY -->
<div class="container">
<div class="row">
    <div class="row col-lg-3 mt-5">  
    <form action="profile.php" method="post" enctype="multipart/form-data">
      <div class="form-group">

        <div class="form-group">
        <label for="exampleInputFirstname">Add Your Firstname</label>
        <input type="text" class="form-control" id="firstname-input" name="firstname-input" value="<?php echo htmlentities($firstname); ?>">
        </div>

       <div class="form-group">
        <label for="exampleInputLastname">Add Your Lastname</label>
        <input type="text" class="form-control" id="lastname-input" name="lastname-input" value="<?php echo $lastname; ?>">
      </div>
      
       <div class="form-group">
       <label for="aboutMe">Add Something About Yourself</label>
        <textarea name="aboutme-input" id="" cols="30" rows="3"><?php echo $aboutMe; ?></textarea>
        </div>

      <label for="exampleFormControlFile1">Add Your Profile Image</label>
        <input type="file" class="form-control-file" id="image" name="image"><br>
    
      <input type="submit" class="btn btn-primary" name="create-profile" value="Create Profile">
      <input type="submit" class="btn btn-primary" name="update-profile" value="Update Profile">

      </div>
    </form>  
    </div>  
    
 <?php } ?>
  
  
  <?php
 

    
if(isset($_POST['create-profile']) || isset($_POST['update-profile'])){
    

     if(isset($_POST['firstname-input']) && isset($_POST['lastname-input']) && isset($_POST['aboutme-input']) && isset($_FILES['image'])){
         
      $post_image = $_FILES['image']['name'];
      $post_image_temp = $_FILES['image']['tmp_name'];
       
     
      $firstname = $_POST['firstname-input']; 
      $lastname = $_POST['lastname-input'];
      $aboutMe = $_POST['aboutme-input'];
       
          move_uploaded_file($post_image_temp,"./images/$post_image");
// IF UPDATING DATA AND DO NOT UPDATING IMAGE - IMAGE STAYS OLD        
         if(empty($post_image)){
        
       $query = "SELECT * FROM login_profile WHERE email = '" . $_SESSION['email'] . "' ";
        
       $select_image = mysqli_query($connection, $query);
        
        while($row = mysqli_fetch_array($select_image)){
            
            $post_image = $row['image'];
        }
        
    }
         
$updateQuery = "UPDATE login_profile SET name='$firstname', lastname='$lastname', about_me ='$aboutMe' , image = '$post_image' WHERE email = '" . $_SESSION['email'] . "' ";
$result = mysqli_query($connection,$updateQuery);
         
       
         
            if(!$result ) {
                
               die('Could not update data: ' . mysqli_error($connection));
            } else {
                
                  header('Location: profile.php');
                
            }
     }
}
         
    ?>
  
<?php
    
      $query = "SELECT * from login_profile WHERE email = '" . $_SESSION['email'] . "' ";
      $select_user_query = mysqli_query($connection, $query);
      if(!$select_user_query){
          die("ERROR ERROR ERROR " . mysqli_error($connection));
      }
      
      while($row = mysqli_fetch_assoc($select_user_query)){
          
          $db_id = $row['id'];
          $name = $row['name'];
          $lastname = $row['lastname'];
          $image = $row['image'];
          $aboutMe = $row['about_me'];
          
?>

<div class="row col-lg-4 offset-lg-1 mt-5 img-container">
     <img class="img-responsive rounded mx-auto d-block cover" src="images/<?php echo $image; ?>" alt="" style="height: 400px; width: 100%; object-fit: cover">
</div>

<div class="row col-lg-4 mt-5 list-group ">
    <h2 class="list-group pl-5">
        <?php echo $name . " " . $lastname;?>
    </h2>
    <label class="mt-5 pl-5" for="About me"><strong>About Me</strong></label>
     <p class="list-group pl-5">
        <?php echo $aboutMe;?>
     </p>
</div>
 
 
  
<?php } ?>

</div>
</div>
<!--UPDATE QUERY -->

<!--FOOTER-->
<footer class="footer navbar-dark bg-primary">
      <div class="container">
        <span class="text-white">Place sticky footer content here.</span>
      </div>
</footer>
<!--FOOTER-->
    
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="script.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>         
                
</body>
</html>